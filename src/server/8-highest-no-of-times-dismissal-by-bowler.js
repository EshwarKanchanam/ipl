const getDataFromCsv = require("./getDataFromCsv");
const fs = require("fs");

function getHighestNumberOfTimesDismissalByPlayer(csvFilePath) {
  getDataFromCsv(csvFilePath)
    .then((deliveriesData) => {
      let highestNumberOfTimesDismissalByPlayer = {};

      for (let delivery of deliveriesData) {
        if (
          highestNumberOfTimesDismissalByPlayer[delivery.bowler] === undefined
        ) {
          highestNumberOfTimesDismissalByPlayer[delivery.bowler] = {};
        }
        // console.log( delivery.dismissal_kind.length);
        if (delivery.dismissal_kind.length != "") {
          if (
            highestNumberOfTimesDismissalByPlayer[delivery.bowler][
              delivery.player_dismissed
            ] === undefined
          ) {
            highestNumberOfTimesDismissalByPlayer[delivery.bowler][
              delivery.player_dismissed
            ] = 1;
          } else {
            highestNumberOfTimesDismissalByPlayer[delivery.bowler][
              delivery.player_dismissed
            ] += 1;
          }
        }
      }
      let highestNoOfTimesABatsmanDismissedByBowler = {};
      let highestNoOfTimesDismissed = 0;
      for (let bowler in highestNumberOfTimesDismissalByPlayer) {
        for (let dismissedBatsman in highestNumberOfTimesDismissalByPlayer[
          bowler
        ]) {
          if (
            highestNoOfTimesDismissed <
            highestNumberOfTimesDismissalByPlayer[bowler][dismissedBatsman]
          ) {
            highestNoOfTimesDismissed =
              highestNumberOfTimesDismissalByPlayer[bowler][dismissedBatsman];
            highestNoOfTimesABatsmanDismissedByBowler.bowler = bowler;
            highestNoOfTimesABatsmanDismissedByBowler.dismissedBatsman =
              dismissedBatsman;
            highestNoOfTimesABatsmanDismissedByBowler.dismissals =
              highestNoOfTimesDismissed;
          }
        }
      }
      fs.writeFileSync(
        "../public/output/getHighestNumberOfTimesDismissalByPlayer.json",
        JSON.stringify(highestNoOfTimesABatsmanDismissedByBowler)
      );
    })
    .catch((error) => console.log(error));
}

getHighestNumberOfTimesDismissalByPlayer("../data/deliveries.csv");
