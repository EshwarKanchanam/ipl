const getDataFromCsv = require("./getDataFromCsv");
const fs = require("fs");

function getExtraRunsPerTeamByYear(csvFilePathForDeliveries, year) {
  getDataFromCsv("../data/matches.csv")
    .then((matches) => {
      let matchIds = [];
      for (let match of matches) {
        if (match.season == year) {
          matchIds.push(match.id);
        }
      }
      getDataFromCsv(csvFilePathForDeliveries)
        .then((deliveriesData) => {
          let extraRunsPerMatch = {};
          for (let id of matchIds) {
            for (let match of deliveriesData) {
              if (id == match.match_id) {
                if (extraRunsPerMatch[match.bowling_team] === undefined) {
                  extraRunsPerMatch[match.bowling_team] = Number(
                    match.extra_runs
                  );
                } else {
                  extraRunsPerMatch[match.bowling_team] += Number(
                    match.extra_runs
                  );
                }
              }
            }
          }

          fs.writeFileSync(
            "../public/output/extraRunsPerTeamByYear.json",
            JSON.stringify(extraRunsPerMatch)
          );
        })
        .catch((error) => console.log(error));
    })
    .catch((error) => console.log(error));
}

getExtraRunsPerTeamByYear("../data/deliveries.csv", 2016);
