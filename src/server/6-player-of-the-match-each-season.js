const getDataFromCsv = require("./getDataFromCsv");
const fs = require("fs");

function getPlayerOfTheMatchEachSeason(csvFilePath) {
  getDataFromCsv(csvFilePath)
    .then((matches) => {
      let playerOfTheMatchBySeason = {};
      for (let match of matches) {
        if (playerOfTheMatchBySeason[match.season] === undefined) {
          playerOfTheMatchBySeason[match.season] = {
            [match.player_of_match]: 1,
          };
        } else {
          if (
            playerOfTheMatchBySeason[match.season][match.player_of_match] ===
            undefined
          ) {
            playerOfTheMatchBySeason[match.season][match.player_of_match] = 1;
          } else {
            playerOfTheMatchBySeason[match.season][match.player_of_match] += 1;
          }
        }
      }
      //   console.log(playerOfTheMatchBySeason);

      let highestNumberOfPlayerOfMatchesBySeason = {};
      for (let season in playerOfTheMatchBySeason) {
        let max = 0;
        let players = playerOfTheMatchBySeason[season];
        for (let player in players) {
          if (players[player] > max) {
            max = players[player];
            highestNumberOfPlayerOfMatchesBySeason[season] = player;
          }
        }
      }
      fs.writeFileSync(
        "../public/output/playerOfTheMatchEachSeason.json",
        JSON.stringify(highestNumberOfPlayerOfMatchesBySeason)
      );
    })
    .catch((error) => console.log(error));
}

getPlayerOfTheMatchEachSeason("../data/matches.csv");
