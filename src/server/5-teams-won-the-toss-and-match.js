const getDataByCsv = require("./getDataFromCsv");
const fs = require("fs");

function getTeamsWhoWonTossAndMatch(csvFilePath) {
  getDataByCsv(csvFilePath)
    .then((matches) => {
      let noOfTimesTeamsWhoWonTossAndMatch = {};
      for (let match of matches) {
        if (match.toss_winner === match.winner) {
          if (
            noOfTimesTeamsWhoWonTossAndMatch[match.toss_winner] === undefined
          ) {
            noOfTimesTeamsWhoWonTossAndMatch[match.toss_winner] = 1;
          } else {
            noOfTimesTeamsWhoWonTossAndMatch[match.toss_winner] += 1;
          }
        }
      }
      fs.writeFileSync(
        "../public/output/teamsWhoWonTossAndMatch.json",
        JSON.stringify(noOfTimesTeamsWhoWonTossAndMatch)
      );
    })
    .catch((error) => console.log(error));
}

getTeamsWhoWonTossAndMatch("../data/matches.csv");
