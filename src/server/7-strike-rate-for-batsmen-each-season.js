const getDataFromCsv = require("./getDataFromCsv");
const fs = require("fs");

function getStrikeRateForBatsmenEachSeason(csvFilePath) {
  getDataFromCsv("../data/matches.csv")
    .then((matches) => {
      let seasons = {};
      for (let match of matches) {
        if (seasons[match.season] === undefined) {
          seasons[match.season] = [];
        }
        seasons[match.season].push(match.id);
      }

      getDataFromCsv(csvFilePath)
        .then((deliveriesData) => {
          let batsmansBySeason = {};
          for (let season in seasons) {
            if (batsmansBySeason[season] === undefined) {
              batsmansBySeason[season] = {};
            }
            for (let matchId of seasons[season]) {
              for (let delivery of deliveriesData) {
                if (delivery.match_id === matchId) {
                  let runs = delivery.total_runs - delivery.extra_runs;
                  if (
                    batsmansBySeason[season][delivery.batsman] === undefined
                  ) {
                    batsmansBySeason[season][delivery.batsman] = {
                      runs,
                      balls: delivery.noball_runs == 0 ? 1 : 0,
                    };
                  } else {
                    batsmansBySeason[season][delivery.batsman].runs += runs;
                    batsmansBySeason[season][delivery.batsman].balls +=
                      delivery.noball_runs == 0 ? 1 : 0;
                  }
                }
              }
            }
          }

          for (let season in batsmansBySeason) {
            for (let batsman in batsmansBySeason[season]) {
              let runs = batsmansBySeason[season][batsman].runs;
              let balls = batsmansBySeason[season][batsman].balls;
              let strikeRate = (runs / balls) * 100;
              batsmansBySeason[season][batsman] =
                Math.round(strikeRate * 100) / 100;
            }
          }
          fs.writeFileSync(
            "../public/output/strikeRateForBatmanEachSeason.json",
            JSON.stringify(batsmansBySeason)
          );
        })
        .catch((error) => console.log(error));
    })
    .catch((error) => console.log(error));
}

getStrikeRateForBatsmenEachSeason("../data/deliveries.csv");
