const getDataFromCsv = require("./getDataFromCsv");
const fs = require("fs");

function matchesWonPerTeamPerYear(csvFilepath) {
  getDataFromCsv(csvFilepath)
    .then((matches) => {
      let matchesWonPerTeamPerYear = {};
      for (let match of matches) {
        if (matchesWonPerTeamPerYear[match.season] === undefined) {
          matchesWonPerTeamPerYear[match.season] = { [match.winner]: 1 };
        } else {
          if (
            matchesWonPerTeamPerYear[match.season][match.winner] === undefined
          ) {
            matchesWonPerTeamPerYear[match.season][match.winner] = 1;
          } else {
            matchesWonPerTeamPerYear[match.season][match.winner] += 1;
          }
        }
      }
      fs.writeFileSync(
        "../public/output/matchesWonPerTeamPerYear.json",
        JSON.stringify(matchesWonPerTeamPerYear)
      );
    })
    .catch((error) => console.log(error));
}

matchesWonPerTeamPerYear("../data/matches.csv");
