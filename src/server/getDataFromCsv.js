const csv = require("csvtojson");

function getDataFromCsv(filePath) {
  return csv()
    .fromFile(filePath)
    .then((jsonObj) => {
      return jsonObj;
    })
    .catch((error) => console.log(error));
}

module.exports = getDataFromCsv;
