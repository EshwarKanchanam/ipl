const getDataByCsv = require("./getDataFromCsv");
const fs = require("fs");

function getTop10EconomicalBowlersByYear(csvFilePathOfDeliveries, year) {
  getDataByCsv("../data/matches.csv")
    .then((matches) => {
      let matchIds = [];
      for (let match of matches) {
        if (match.season == year) {
          matchIds.push(match.id);
        }
      }

      getDataByCsv(csvFilePathOfDeliveries)
        .then((deliveriesData) => {
          let bowlers = {};
          for (let id of matchIds) {
            for (let delivery of deliveriesData) {
              if (delivery.match_id == id) {
                if (bowlers[delivery.bowler] === undefined) {
                  bowlers[delivery.bowler] = {
                    balls: 1,
                    runs: Number(delivery.total_runs),
                  };
                } else {
                  bowlers[delivery.bowler].balls += 1;
                  bowlers[delivery.bowler].runs += Number(delivery.total_runs);
                }
              }
            }
          }

          let bowlersWithEconomy = {};
          let BALLS_PER_OVER = 6;
          for (let bowler in bowlers) {
            let overs = bowlers[bowler].balls / BALLS_PER_OVER;
            let economy = bowlers[bowler].runs / overs;
            bowlersWithEconomy[bowler] = Math.round(economy * 10) / 10;
          }

          let bowlersWithEconomyArray = Object.entries(bowlersWithEconomy);
          bowlersWithEconomyArray.sort((bowler1, bowler2) => {
            return bowler1[1] - bowler2[1];
          });

          let topTenBowlers = bowlersWithEconomyArray.slice(0, 10);

          fs.writeFileSync(
            "../public/output/top10EconomicalBowlers.json",
            JSON.stringify(topTenBowlers)
          );
        })
        .catch((error) => console.log(error));
    })
    .catch((error) => console.log(error));
}

getTop10EconomicalBowlersByYear("../data/deliveries.csv", 2015);
