const getDataFromCsv = require("./getDataFromCsv");
const fs = require("fs");

function getBestEconomyBowlerInSuperOver(csvFilePath) {
  getDataFromCsv(csvFilePath)
    .then((deliveriesData) => {
      let economiesOfBowlerInSuperOver = {};
      let isSuperOver = false;
      for (let delivery of deliveriesData) {
        if (
          delivery.is_super_over != 0 &&
          economiesOfBowlerInSuperOver[delivery.bowler] == undefined
        ) {
          economiesOfBowlerInSuperOver[delivery.bowler] = {};
        }
        isSuperOver = delivery.is_super_over != 0;
        if (isSuperOver) {
          let runs =
            delivery.total_runs -
            delivery.bye_runs -
            delivery.legbye_runs -
            delivery.penalty_runs;
          economiesOfBowlerInSuperOver[delivery.bowler].runs =
            economiesOfBowlerInSuperOver[delivery.bowler].runs == undefined
              ? runs
              : economiesOfBowlerInSuperOver[delivery.bowler].runs + runs;
          economiesOfBowlerInSuperOver[delivery.bowler].balls =
            economiesOfBowlerInSuperOver[delivery.bowler].balls == undefined
              ? delivery.wide_runs == 0 && delivery.noball_runs == 0
                ? 1
                : 0
              : economiesOfBowlerInSuperOver[delivery.bowler].balls +
                (delivery.wide_runs == 0 && delivery.noball_runs == 0 ? 1 : 0);
        }
      }

      for (let bowler in economiesOfBowlerInSuperOver) {
        let BALLS_PER_OVER = 6;
        let overs = economiesOfBowlerInSuperOver[bowler].balls / BALLS_PER_OVER;
        let economy = economiesOfBowlerInSuperOver[bowler].runs / overs;
        economiesOfBowlerInSuperOver[bowler] = economy;
      }

      let bestEconomyBowler = {};
      let economy = Number.MAX_SAFE_INTEGER;
      for (let bowler in economiesOfBowlerInSuperOver) {
        if (economy > economiesOfBowlerInSuperOver[bowler]) {
          bestEconomyBowler.bowler = bowler;
          bestEconomyBowler.economy = economiesOfBowlerInSuperOver[bowler];
          economy = economiesOfBowlerInSuperOver[bowler];
        }
      }
      fs.writeFileSync(
        "../public/output/getBestEconomyBowlerInSuperOver.json",
        JSON.stringify(bestEconomyBowler)
      );
    })
    .catch((error) => console.log(error));
}

getBestEconomyBowlerInSuperOver("../data/deliveries.csv");
