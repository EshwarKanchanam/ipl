const getDataFromCsv = require("./getDataFromCsv");
const fs = require("fs");

function getDrawMatchesPerSeason(csvFilePath) {
  getDataFromCsv(csvFilePath)
    .then((matches) => {
      let drawMatchesPerSeason = {};
      for (let match of matches) {
        if (drawMatchesPerSeason[match.season] === undefined) {
          drawMatchesPerSeason[match.season] = [];
        }
        if (match.winner === "") {
          drawMatchesPerSeason[match.season] = [
            ...drawMatchesPerSeason[match.season],
            [match.team1, match.team2],
          ];
        }
      }
      fs.writeFileSync(
        "../public/output/drawMatchesPerSeason.json",
        JSON.stringify(drawMatchesPerSeason)
      );
    })
    .catch((error) => console.log(error));
}

getDrawMatchesPerSeason("../data/matches.csv");
