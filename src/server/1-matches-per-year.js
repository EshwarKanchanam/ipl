const getDataFromCsv = require("./getDataFromCsv");
const fs = require("fs");

function matchesPerYear(csvFilepath) {
  getDataFromCsv(csvFilepath)
    .then((matchesData) => {
      let matchesPerYear = {};
      for (let match of matchesData) {
        matchesPerYear[match.season] = matchesPerYear[match.season]
          ? matchesPerYear[match.season] + 1
          : 1;
      }
      fs.writeFileSync(
        "../public/output/matchesPerYear.json",
        JSON.stringify(matchesPerYear)
      );
    })
    .catch((error) => console.log(error));
}

matchesPerYear("../data/matches.csv");
